#ifndef ANACONST_H
#define ANACONST_H

const double hplanck = 4.135667696e-15; // eV*s
const double clight = 299792458; //m/s
const double pi = acos(-1);
const double m2tobarn = 1e-28;

const double eRadius = 2.817940285e-15;  // classical electron radius (m)
const double eMass = 0.510998902e+6;   // electron mass (eV)
const double gLambda = 532e-9; //m
const double gEnergy = hplanck * clight / gLambda; //eV

double eEnergy = 18e9; //eV
double Gamma = eEnergy / eMass;
double a = 1 / ( 1 + 4*Gamma*gEnergy/eMass);//
double gMaxEnergy = 4*a*gEnergy*std::pow(Gamma,2)/1e9;//GeV

void recalcMaxEnergy(){
  Gamma = eEnergy / eMass;
  a = 1 / ( 1 + 4*Gamma*gEnergy/eMass);//
  gMaxEnergy = 4*a*gEnergy*std::pow(Gamma,2)/1e9;//GeV
}


const string xSecNm[4]={"uX","pX","uXa","pXa"};
const string xSecTi[4]={"#sigma_{unpol}","#sigma_{pol}","#sigma_{unpol}O(#alpha)","#sigma_{pol}O(#alpha)"};
TH1D *hXsec[4], *iXsec[4];

const int nProj = 4;
const double zProj[nProj]={6000,10000,25000,40000};//mm
const int nWght = 5;//[counts,upolXsec, asym, E, asym*E]
const string wghtTit[nWght]={"counts","unpolXsec","polXsec","energy*unpolXsec","energy*polXsec"};
const int n2Dbins=800;
const double partBinLimit[2]={10,0.5};
TH2D *xy[2][nProj][nWght];//[g/e]
/* TH2D *xyP[2][nProj][nWght];//[g/e] */
/* TH2D *xyM[2][nProj][nWght];//[g/e] */


#endif
