# Compton Generator

Compton generator written by M. Swartz (hep-ph/9711447). 

## Getting the code
If you just want to run the code:
 ```
 git clone https://gitlab.com/eic/mceg/comptonRad
 cd comptonRad
 make 
```

## Tested system
Compiles with ** GNU Fortran (GCC) 9.2.0**. Warnings need to be fixed as there are common blocks that have different sizes in different parts of the code but the cursory inspection showed that it should not affect the output. Outputs were also compared to an order gfortran version and they were consistent.

## Notes
Right now the generator has elements hard coded in the common block at the beginning of the main routine (**comrad.f** file see line 78 and below). You will need to change those to the correct values for your configuration. Current values: 18 GeV electrons vertically polarized, 2.33eV photons circularly polarized, 500000 events.

The output is streamed to stdout so you may want to pipe it to a file. Each line is an event that contains 12 numbers:
* first 3 are the x, y, z momentum of the photon
* 4th is the photon energy
* 5th is the unpolarized x-section
* 6th is the polarized x-section
* 7th is the unpolarized x-section alpha correction
* 8th is the polarized x-section alpha correction
* 9th-11th are the x, y, z momentum of the electron
* 12th is the electron energy

An basic implementation and use of comRad files into a G4 simulation can be found in the [comptonEIC](https://github.com/cipriangal/comptonEic) package (see [EventReader](https://github.com/cipriangal/comptonEic/blob/master/src/EventReader.cxx#L140)).



