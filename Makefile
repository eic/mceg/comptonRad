EXE = main.exe

FC = gfortran -g 

all: $(EXE)

comrad.o: comrad.f
	$(FC) -c -std=legacy comrad.f
ddilog.o: ddilog.f
	$(FC) -c -std=legacy ddilog.f
ranmar.o: ranmar.f
	$(FC) -c -std=legacy ranmar.f

main.exe: ddilog.o ranmar.o comrad.o 
	$(FC)   -o $(EXE) ddilog.o ranmar.o comrad.o 

clean:
	rm -rf *.o main.exe
